package com.example.thomasbenichou.newsapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;

public class NewActivity extends AppCompatActivity {
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);
        webView = (WebView) findViewById(R.id.webView);

        String htmlContent = getIntent().getExtras().getString("newsContent");
        String newsTitle = getIntent().getExtras().getString("newsTitle");
        webView.loadData(htmlContent, "text/html; charset=UTF-8", null);
        setTitle(newsTitle);
    }
}
