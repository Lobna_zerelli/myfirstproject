package com.example.thomasbenichou.newsapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.oc.rss.fake.FakeNews;
import com.oc.rss.fake.FakeNewsList;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView newsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        List<FakeNews> list = FakeNewsList.all;
        newsList = (RecyclerView) findViewById(R.id.newsList);
        newsList.setLayoutManager(new LinearLayoutManager(this));
        newsList.setAdapter(new NewsAdapter());
    }
}
