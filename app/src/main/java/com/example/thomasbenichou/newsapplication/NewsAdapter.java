package com.example.thomasbenichou.newsapplication;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oc.rss.fake.FakeNews;
import com.oc.rss.fake.FakeNewsList;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {
    List<FakeNews> list = FakeNewsList.all;

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.list_cell, viewGroup, false);
        Log.d("MyViewHolder", "onCreateViewHolder: " + i);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.display(list.get(i).title, i);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView title;
        private int pos = 0;
        private Pair<String, String> currentPair;

        public MyViewHolder(final View itemView) {
            super(itemView);

            title = ((TextView) itemView.findViewById(R.id.title));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context context = view.getContext();
                    Log.d("MyViewHolder", "onClick: " + pos);
                    Intent intent = new Intent(context, NewActivity.class);
                    intent.putExtra("newsContent", list.get(pos).htmlContent);
                    intent.putExtra("newsTitle", list.get(pos).title);
                    context.startActivity(intent);
                }
            });
        }

        public void display(String pair, int position) {
            pos = position;
            title.setText(pair);
        }
    }
}